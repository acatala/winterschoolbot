## intent:greet
- hey
- hola
- buenos dias
- buenas tardes
- buenas

## intent:goodbye
- adios
- hasta luego
- nos vemos
- nos vemos luego
- nos vemos mas tarde
- hablamos
- hablamos mas tarde
- ciao

## intent:thanks
- gracias
- muchas gracias

## intent:affirm
- si
- por supuesto
- eso suena bien
- correcto

## intent:deny
- no
- nunca
- no creo
- no me gusta
- de ningun modo
- no realmente

## intent:mood_great
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good

## intent:mood_unhappy
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremely sad
- so sad

## intent:course_info
- puedes darme informacion sobre el curso [affective computing](subject)?
- dame informacion sobre el curso [Software and Sustainability](subject)
- dime informacion del curso [Persuasive System Design](subject)
- sobre que va el curso [natural language processing](subject)?
- sobre que va [Creativity in Software Engineering](subject)?
- explicame un poco sobre [UI Design](subject)?
- cuentame algo sobre [Software and Sustainability](subject)?
- de que va [Creativity in Software Engineering](subject)?
- de que va el curso [Software and Sustainability](subject)?
- de que va la asignatura [UI Design](subject)?
- dime de que va [Natural Language Processing](subject)?

## intent:course_timetable
- cuando sera el curso [affective computing](subject)?
- cuando se celebrara la asignatura [Creativity in Software Engineering](subject)?
- a que hora sera el curso [UI Design](subject)?
- cuando se celebra el curso [Persuasive System Design](subject)?
- dime la hora programada para el curso [Creativity in Software Engineering](subject)
- cuando se imparte el curso [Natural Language Processing](subject)?
- hora de [Creativity in Software Engineering](subject)?
- horas del curso [affective computing](subject)?
- cual es el horario del curso [affective computing](subject)?
- que horario tiene [Creativity in Software Engineering](subject)?
- dime el horario que tiene el curso [Software and Sustainability](subject)?
- cual es el horario de [Natural Language Processing](subject)?
- que horario tiene la asignatura [Software and Sustainability](subject)?
- dime el horario que tiene la asignatura [Persuasive System Design](subject)?


## intent:course_teacher
- quien imparte el curso [affective computing](subject)?
- quien es el profesor del curso [UI Design](subject)?
- dime quien imparte el curso [Natural Language Processing](subject)
- dime el nombre del profesor de la asignatura [Software and Sustainability](subject)?
- dime quien presenta [Creativity in Software Engineering](subject)?
- como se llama el profesor de [UI Design](subject)?
- quien dicta el curso [Natural Language Processing](subject)?
- dime quien dicta el curso [affective computing](subject)
- dime el nombre del profesor de la asignatura [Software and Sustainability](subject)?
- dime quien presenta [Creativity in Software Engineering](subject)?
- como se llama el profesor de [affective computing](subject)?

## intent:course_duration
- cual es la duracion de los cursos?
- que duracion tienen los cursos?
- dime cual es la duracion de los cursos
- cuanto duran

## intent:what_courses
- que cursos?
- que asignaturas hay?
- que cursos tiene la escuela de invierno?
- me pregunto que cursos hay
- dime que cursos hay
- dime que asignaturas hay
- dime que cursos tiene la escuela de invierno
- dime que asignaturas tiene la escuela de invierno
- dame informacion basica
- sobre que es la escuela de invierno
- dime la tematica de la escuela de verano

## intent:where_winterschool
- donde tendra lugar la escuela de invierno?
- donde sera la escuela de invierno?
- donde se imparte la escuela de invierno?
- dime donde se imparte la escuela de invierno
- dime donde se imparte
- puede decirme donde se imparte la escuela de invierno?
- podria decirme donde sera la escuela de invierno?

## intent:number_courses
- cuantos cursos?
- cuantos cursos tiene la escuela de invierno?
- hay muchos cursos en la escuela de invierno?
- hay muchos cursos?
- me pregunto cuantos cursos hay
- dime cuantos cursos hay
- dime cuantos cursos tiene la escuela de invierno

## intent:winterschool_timespan
- cuanto dura la escuela de invierno?
- cuantos dias toma la escuela de invierno?
- dime cuando tendra lugar la escuela de invierno
- por cuantos dias se extendera la escuela de invierno?
- cual es la duracion de la escuela de invierno?
- dime la duracion de la escuela de invierno
- dime que dias tendra lugar la escuela de invierno
- dime que dias tendra lugar los cursos
- dime la duracion de los cursos
- por cuantos dias se extendera la escuela de invierno?

## intent:recommend_winterschool_tofriend
- me gustaria recomendar la escuela de invierno a un amigo
- quisiera recomendar la escuela de invierno a un amigo
- quisiera recomendar la proxima edicion de la escuela de invierno a un amigo
- quisiera recomendar la proxima edicion a un amigo
- me gustaria recomendaros
- quisiera recomendar la proxima edicion a un amigo

## intent:feedback_course
- me gustaria dar retroalimentacion
- quisiera dar retroalimentacion
- me gustaria dar feedback
- quisiera dar feedback
- me gustaria enviar retroalimentacion
- quisiera enviar retroalimentacion
- me gustaria enviar feedback
- quisiera enviar feedback
- me gustaria dar retroalimentacion sobre [affective computing](subject)
- quisiera dar retroalimentacion de [Software and Sustainability](subject)
- me gustaria dar feedback para [Persuasive System Design](subject)
- quisiera dar feedback a [natural language processing](subject)
- me gustaria enviar retroalimentacion para la asignatura [Creativity in Software Engineering](subject)
- quisiera enviar retroalimentacion para el curso [UI Design](subject)
- me gustaria enviar feedback al curso [Software and Sustainability](subject)
- quisiera enviar feedback sobre [Natural Language Processing](subject)


## intent:request_help_skills
- me gustaria saber que puedes hacer
- que puedes hacer?
- como puedes ayudar?
- como puedes ayudarme?
- en que puedes ayudar?
- que sabes hacer?
- que puedes responder?
- dime en que puedes ayudar
- que sabes?
- informa de lo que sabes hacer
- dime que puedes hacer
- informa sobre tus habilidades

## intent:stop
- Olvidalo
- Cancelar
- Cancela
- parar
- para
- Dejalo


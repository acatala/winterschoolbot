## happy path
* greet
  - utter_greet
* mood_great
  - utter_happy

## sad path 1
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* affirm
  - utter_happy

## sad path 2
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* deny
  - utter_goodbye

## say goodbye
* goodbye
  - utter_goodbye

## bot challenge
* bot_challenge
  - utter_iamabot
  
## winterschool timespan
* winterschool_timespan
  - utter_winterschool_timespan

## number of courses
* number_courses
  - utter_number_courses

## what courses
* what_courses
  - utter_what_courses
  
## course duration
* course_duration
  - utter_course_duration
  
## course teacher
* course_teacher
  - action_course_teacher

## timetable
* course_timetable
  - utter_course_timetable
  
## course info
* course_info
  - action_course_info

## where winterschool
* where_winterschool
  - utter_where_winterschool
    
## rate and recommend school to a friend
* recommend_winterschool_tofriend
   - recommend_afriend_form
   - form{"name": "recommend_afriend_form"}
   - form{"name": null}

## feeback course
* feedback_course
   - feedback_course_form
   - form{"name": "feedback_course_form"}
   - form{"name": null}

## feeback course with stop
* feedback_course
   - feedback_course_form
   - form{"name": "feedback_course_form"}
* stop
    - utter_ask_continue
* deny
    - action_deactivate_form
    - form{"name": null}
    
## request help skills
* request_help_skills
   - utter_inform_help_skills



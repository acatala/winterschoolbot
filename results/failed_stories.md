## happy path 2 (/tmp/tmpm547446z/1c5b35b196a34461a090ed2f257a62a1_conversation_tests.md)
* greet: hello there!
    - utter_greet
* mood_great: amazing
    - utter_happy
* goodbye: bye-bye!   <!-- predicted: mood_unhappy: bye-bye! -->
    - utter_goodbye   <!-- predicted: utter_cheer_up -->


## sad path 1 (/tmp/tmpm547446z/1c5b35b196a34461a090ed2f257a62a1_conversation_tests.md)
* greet: hello
    - utter_greet
* mood_unhappy: not good
    - utter_cheer_up
    - utter_did_that_help
* affirm: yes   <!-- predicted: greet: yes -->
    - utter_happy   <!-- predicted: utter_greet -->


## sad path 2 (/tmp/tmpm547446z/1c5b35b196a34461a090ed2f257a62a1_conversation_tests.md)
* greet: hello
    - utter_greet
* mood_unhappy: not good
    - utter_cheer_up
    - utter_did_that_help
* deny: not really   <!-- predicted: mood_unhappy: not really -->
    - utter_goodbye   <!-- predicted: utter_cheer_up -->


## say goodbye (/tmp/tmpm547446z/1c5b35b196a34461a090ed2f257a62a1_conversation_tests.md)
* goodbye: bye-bye!   <!-- predicted: mood_unhappy: bye-bye! -->
    - utter_goodbye   <!-- predicted: utter_cheer_up -->


## bot challenge (/tmp/tmpm547446z/1c5b35b196a34461a090ed2f257a62a1_conversation_tests.md)
* bot_challenge: are you a bot?   <!-- predicted: mood_unhappy: are you a bot? -->
    - utter_iamabot   <!-- predicted: utter_cheer_up -->



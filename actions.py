# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
#
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import AllSlotsReset, FollowupAction, SlotSet
from rasa_sdk.forms import FormAction
#
#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []


class DB ():
    
    subject_list = ['ui design', 'affective computing', 'natural language processing', 'software and sustainability', 'persuasive system design', 'creativity in software engineering']
    
    teacher_by_subject_dict = {}
    teacher_by_subject_dict['ui design']="Alejandro Catala"
    teacher_by_subject_dict['affective computing']="Dirk Heylen"
    teacher_by_subject_dict['natural language processing']="Ricardo Gacitúa"
    teacher_by_subject_dict['software and sustainability']="Nelly Condori"
    teacher_by_subject_dict['persuasive system design']="Harri Oinas"
    teacher_by_subject_dict['creativity in software engineering']="Andrea Herrmann"
    
    desc_by_subject_dict = {}
    desc_by_subject_dict['ui design']="UI Design se centra en aspectos interactivos y practicos de chatbots"
    desc_by_subject_dict['affective computing']="Computation Afectiva versa sobre los aspectos computacionales de las emociones"
    desc_by_subject_dict['natural language processing']="NLP consiste de una breve introduccion al procesamiento de lenguaje"
    desc_by_subject_dict['software and sustainability']="Esta es la decripcion breve de S&S"
    desc_by_subject_dict['persuasive system design']="Esta es la descripcion breve de PSD"
    desc_by_subject_dict['creativity in software engineering']="Esta es la descripcion breve de Creativity in SE"
    
                
    def get_teacher_by_subject(subject):
        subject_key = subject.lower()
        if subject_key in DB.teacher_by_subject_dict.keys():
            return DB.teacher_by_subject_dict[subject_key]
        else:
            return "NOT_FOUND"
            
            
    def get_description_by_subject(subject):
        subject_key = subject.lower()
        if subject_key in DB.desc_by_subject_dict.keys():
            return DB.desc_by_subject_dict[subject_key]
        else:
            return "NOT_FOUND"
        



class ActionCourseTeacher(Action):

     def name(self) -> Text:
         return "action_course_teacher"

     def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
         raw_subject=""
#         value=tracker.latest_message['entities'][0]['value']
         value=None
         for e in tracker.latest_message["entities"]:
            if e['entity'] == 'subject':
                value=e['value']
         if value != None:
            raw_subject=str(value)
            print(raw_subject)
         teacher = DB.get_teacher_by_subject(raw_subject)
         ret_text = ""
         if teacher is "NOT_FOUND":
            ret_text = "uhm. Has escrito la asignatura correctamente? No la encuentro."
         else:
            ret_text = "El profesor/a del curso " + raw_subject + " es " + teacher + "."
         
         dispatcher.utter_message(text=ret_text)

         return []
         
class ActionCourseInfo(Action):

     def name(self) -> Text:
         return "action_course_info"

     def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

         raw_subject=""
         value=tracker.latest_message['entities'][0]['value']
         if value != None:
            raw_subject=value
            print(raw_subject)
         description = DB.get_description_by_subject(raw_subject)
         ret_text = ""
         if description is "NOT_FOUND":
            ret_text = "uhm. Has escrito la asignatura correctamente? No la encuentro."
         else:
            ret_text = "Esta es la descripcion informativa del curso " + raw_subject + ": " + description + "."
         
         dispatcher.utter_message(text=ret_text)

         return []

class RecommendAFriendForm(FormAction):
    """Example of a custom form action for recommeding a course to a friend"""

    def name(self):
        """Unique identifier of the form"""
        return "recommend_afriend_form"


    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""
        return ["friend_name", "friend_email", "short_reason", "course_rating"]

    def slot_mappings(self):
        return {
            "friend_name": self.from_text(),
            "friend_email": self.from_text(),
            "short_reason": self.from_text(),
            "course_rating": self.from_text()
        }


    def submit(self, dispatcher: CollectingDispatcher, tracker: Tracker,  domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        """Define what the form has to do
            	after all required slots are filled"""
       	friend_name = tracker.get_slot("friend_name")
       	friend_email = tracker.get_slot("friend_email")
       	short_reason = tracker.get_slot("short_reason")
       	course_rating = tracker.get_slot("course_rating")       	
#       	dispatcher.utter_template('utter_recommendation_sent', tracker)
        dispatcher.utter_message(template="utter_recommendation_sent", friend_name=friend_name, friend_email=friend_email, short_reason=short_reason, course_rating=course_rating)
        return [AllSlotsReset()]

class FeedbackCourseForm(FormAction):
    """Example of a custom form action for giving feedback of a course"""

    def name(self):
        """Unique identifier of the form"""
        return "feedback_course_form"


    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""
        return ["course_name", "short_feedback"]

    def slot_mappings(self):
        return {
            "course_name": [self.from_entity(entity="subject"), self.from_text(not_intent = "stop")],
            "short_feedback": self.from_text()            
        }
        
    def validate_course_name(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate course_name value."""

        if value.lower() in DB.subject_list:
            # validation succeeded, set the value of the "cuisine" slot to value
            return {"course_name": value}
        else:
            dispatcher.utter_message(template="utter_wrong_subject")
            # validation failed, set this slot to None, meaning the
            # user will be asked for the slot again
            return {"course_name": None}



    def submit(self, dispatcher: CollectingDispatcher, tracker: Tracker,  domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        """Define what the form has to do
            	after all required slots are filled"""

       	course_name = tracker.get_slot("course_name")
       	short_feedback = tracker.get_slot("short_feedback")
        dispatcher.utter_message(template="utter_coursefeedback_sent", course_name=course_name, short_feedback=short_feedback)
       	#dispatcher.utter_template('utter_coursefeedback_sent', tracker)


        return [AllSlotsReset()]

